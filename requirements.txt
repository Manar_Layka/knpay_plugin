django-jsonfield==1.4.0
django-appconf==1.0.2
django-fsm==2.6.1
requests==2.22.0
six>=1.11
coverage==4.5.2
