=====
TODOs
=====

- Integrate KNPay payment request API.
- Finalize arabic translation.
- Add unittests.
- Write more extensive docs and include mode hooking examples
- Send signals on changing states? To be analyzed.
- Drop RENDER_FORM and find a better way to manage the logic related.
- Add possibility to create a new payment transaction from existing one?